.. _swh-docs:

Software Heritage documentation
===============================

.. toctree::
  :maxdepth: 2
  :caption: Contents:

  devel/index
  sysadm/index
  user/index

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`routingtable`
* :ref:`search`
* :ref:`glossary`
