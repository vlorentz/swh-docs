.. _swh-docs-user:

Software Heritage - User Documentation
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   faq/index
   listers/index
   loaders/index
   save_code_now/webhooks/index

.. only:: user_doc

   Indices and tables
   ------------------

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
