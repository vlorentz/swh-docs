.. _swh-docs-sysadm:

Software Heritage - Sysadmin Documentation
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting-started/index
   puppet/index
   network-architecture/index
   server-architecture/index
   data-silos/index
   deployment/index
   user-management/index
   life-cycle-management/index
   mirror-operations/index
   support-services/index

.. only:: sysadm_doc

   Indices and tables
   ------------------

   * :ref:`genindex`
   * :ref:`search`
